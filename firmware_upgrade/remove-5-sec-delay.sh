#!/bin/bash

set -e 

FAIL="\033[31m[-]\033[00m"
GOOD="\033[32m[+]\033[00m"
INFO="\033[34m[*]\033[00m"
INFO2="\033[33m[?]\033[00m"

FIRMWARE_URL="https://raw.githubusercontent.com/micronucleus/micronucleus/v1.11/upgrade/releases/micronucleus-1.11-entry-jumper-pb0-upgrade.hex"
FIRMWARE="micronucleus-1.11-entry-jumper-pb0-upgrade.hex"
MICRONUCLEUS_GIT="https://github.com/micronucleus/micronucleus"

printf "${INFO} Installing libusb-dev\n"
sudo apt install libusb-dev

printf "${INFO} Cloning ${MICRONUCLEUS_GIT}\n"
git clone --quiet "${MICRONUCLEUS_GIT}" "$(pwd)/micronucleus"

printf "${INFO} Downloading ${FIRMWARE}\n"
wget -q --show-progress -nc "${FIRMWARE_URL}" -O "${FIRMWARE}"

printf "${INFO} Building micronucleus...\n"
cd micronucleus/commandline && make && cd ../..
micronucleus/commandline/micronucleus --run $FIRMWARE
